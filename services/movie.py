from entidades.movie import Pelicula as MovieModel
from typing import Optional
from sqlalchemy import func


class MovieService:
    def __init__(self, db) -> None:
        self.db = db

    def get_movies(self):
        result = self.db.query(MovieModel).all()
        return result

    def get_movie(self, id: int):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        return result

    def get_movies_by_category(self, category: str, year: Optional[int] = None):
        category = category.lower()
        print(year)
        if not year:
            print(category)
            result = (
                self.db.query(MovieModel)
                .filter(func.lower(MovieModel.category) == category)
                .all()
            )
        else:
            print("hola")
            result = (
                self.db.query(MovieModel)
                .filter(
                    func.lower(MovieModel.category) == category, MovieModel.year == year
                )
                .all()
            )
        return result

    def create_movie(self, movie: MovieModel):
        new_movie = MovieModel(
            **movie.model_dump()
        )  # Mapeamos lo que llega al modelo bsae
        self.db.add(new_movie)
        self.db.commit()
        return

    def update_movie(self, id: int, data: MovieModel):
        movie = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        movie.title = data.title
        movie.overview = data.overview
        movie.year = data.year
        movie.rating = data.rating
        movie.category = data.category
        self.db.commit()
        return

    def delete_movie(self, id: int):
        movie = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        self.db.delete(movie)
        self.db.commit()
        return
