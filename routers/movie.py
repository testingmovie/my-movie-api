from fastapi import Path, Query, HTTPException, status, Depends
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from typing import List
from schemas.movies import Movie
from entidades.movie import Pelicula
from config.database import Session
from middlewares.jwt_bearer import JWTBearer
from fastapi import APIRouter
from services.movie import MovieService

movie_router = APIRouter()

movies = [
    {
        "id": 1,
        "title": "Avatar",
        "overview": "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        "year": "2009",
        "rating": 7.8,
        "category": "Accion",
    },
    {
        "id": 2,
        "title": "La leyenda de Ang",
        "overview": "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        "year": "2009",
        "rating": 7.8,
        "category": "Aventura",
    },
]


@movie_router.get(
    "/movies",
    tags=["movies"],
    response_model=List[Movie],
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(JWTBearer())],
)
def get_movies() -> List[Movie]:
    db = Session()
    # result = db.query(Pelicula).all()  # Dame todos los datos de esta tabla
    result = MovieService(db).get_movies()
    return JSONResponse(
        status_code=status.HTTP_200_OK, content=jsonable_encoder(result)
    )


@movie_router.get(
    "/movies/{id}",
    tags=["movies"],
    response_model=Movie,
    status_code=status.HTTP_200_OK,
)  # Path parameter
def get_movie(
    id: int = Path(ge=1, le=2000)
) -> Movie:  # Los path parameters también se pueden validar
    db = Session()
    # result = db.query(Pelicula).filter(Pelicula.id == id).first()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={"message": "Movie not found"},
        )
    return JSONResponse(
        status_code=status.HTTP_200_OK, content=jsonable_encoder(result)
    )


@movie_router.get(
    "/movies/",
    tags=["movies"],
    response_model=List[Movie],
    status_code=status.HTTP_200_OK,
)  # Query parameter, sino especifico me va a tomar los demás como parametros query
def get_movies_by_category(
    category: str = Query(min_length=5, max_length=15), year: int | None = None
) -> List[Movie]:  # Sin embargo en la función se especifica como entrada
    db = Session()
    # result = db.query(Pelicula).filter(func.lower(Pelicula.category) == category).all()
    result = MovieService(db).get_movies_by_category(category, year)
    if not result:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "message": f"Not found movies with '{category}' category and year '{year}' "
                if year
                else f"Not found movies with '{category}' category"
            },
        )
    return JSONResponse(
        status_code=status.HTTP_200_OK, content=jsonable_encoder(result)
    )
    movies_by_category = []
    for movie in movies:
        if year:
            if movie["category"].lower() == category and int(movie["year"]) == year:
                movies_by_category.append(movie)
        else:
            if movie["category"].lower() == category:
                movies_by_category.append(movie)
    if movies_by_category:
        return JSONResponse(status_code=status.HTTP_200_OK, content=movies_by_category)
    return JSONResponse(
        status_code=status.HTTP_404_NOT_FOUND, content={"message": "Movie not found"}
    )


@movie_router.post(
    "/movies", tags=["movies"], response_model=dict, status_code=status.HTTP_201_CREATED
)
def create_movie(movie: Movie) -> dict:
    # Verifica si ya existe una película con el mismo ID
    db = Session()
    # new_movie = Pelicula(**movie.model_dump())
    # db.add(new_movie)
    # db.commit()

    # if any(m["id"] == movie.id for m in movies):
    #   raise HTTPException(status_code=400, detail="Movie with this ID already exists")

    # Convierte el objeto Pydantic a un diccionario y lo añade a la lista
    # movie_dict = movie.model_dump()
    # movies.append(movie_dict)
    MovieService(db).create_movie(movie)
    return JSONResponse(
        status_code=status.HTTP_201_CREATED, content={"message": "Movie created"}
    )


@movie_router.put(
    "/movies/{id}",
    tags=["movies"],
    response_model=dict,
    status_code=status.HTTP_202_ACCEPTED,
)
def update_movie(id: int, movie: Movie) -> dict:
    db = Session()
    # result = db.query(Pelicula).filter(Pelicula.id == id).first()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={"message": "Movie not found"},
        )
    # result.title = movie.title
    # result.overview = movie.overview
    # result.year = movie.year
    # result.rating = movie.rating
    # result.category = movie.category
    # db.commit()
    MovieService(db).update_movie(id, movie)
    return JSONResponse(
        status_code=status.HTTP_202_ACCEPTED, content={"message": "Movie updated"}
    )

    # Cuando no se hacía con DB
    for item in movies:
        print(item["id"])
        if item["id"] == id:
            item["title"] = movie.title
            item["overview"] = movie.overview
            item["year"] = movie.year
            item["rating"] = movie.rating
            item["category"] = movie.category
            return JSONResponse(
                status_code=status.HTTP_202_ACCEPTED,
                content={
                    "message": "Se ha modificado la pelicula identificada con ID: {}".format(
                        id
                    )
                },
            )
    return JSONResponse(
        status_code=status.HTTP_404_NOT_FOUND, content={"message": "Movie not found"}
    )


@movie_router.delete("/movies/{id}", tags=["movies"], response_model=dict)
def delete_movie(id: int) -> dict:
    db = Session()
    result = db.query(Pelicula).filter(Pelicula.id == id).first()
    if not result:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={"message": "Movie not found"},
        )
    # db.delete(result)
    # db.commit()
    result = MovieService(db).delete_movie(id)
    return JSONResponse(
        status_code=status.HTTP_200_OK, content={"message": "Movie deleted"}
    )
    # movies.remove(movie)  # Esto es para eliminarlo de la lista de peliculas, pero no se puede hacer con la bd, porque no se puede eliminar de la tabla de la bd, solo de la lista de peliculas.
    # return JSONResponse(
    #    status_code=status.HTTP_200_OK, content={"message": "Movie deleted"}
    # )
    # return JSONResponse(
    #    status_code=status.HTTP_404_NOT_FOUND, content={"message": "Movie not found"}
    # )
    # return JSONResponse(
    #    status
    # cuando se hacía sin db
    for movie in movies:
        if movie["id"] == id:
            movies.remove(movie)
            return JSONResponse(
                status_code=status.HTTP_200_OK,
                content={"message": "Se ha eliminado la pelicula: {}".format(movie)},
            )
    return JSONResponse(
        status_code=status.HTTP_404_NOT_FOUND, content={"message": "Movie not found"}
    )
