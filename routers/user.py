from fastapi import APIRouter, status
from utils.jwt_manager import create_token
from fastapi.responses import JSONResponse
from schemas.usuario import User

user_router = APIRouter()


@user_router.post("/login", tags=["auth"])
def login(user: User) -> dict:
    if user.email == "admin@gmail.com" and user.password == "admin":
        token: str = create_token(user.model_dump())
        return JSONResponse(status_code=status.HTTP_200_OK, content={"token": token})
    return JSONResponse(
        status_code=status.HTTP_401_UNAUTHORIZED, content={"token": user}
    )
