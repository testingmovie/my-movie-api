from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from dotenv import load_dotenv
from config.database import Engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router

load_dotenv()  # Cargamos el ambiente


app = FastAPI()  # Creamos la instancia
app.title = "Movie con FastAPI"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)  # Agregamos el middleware
app.include_router(movie_router)
app.include_router(user_router)
Base.metadata.create_all(bind=Engine)  # Creamos la bd

# set_api_key()


@app.get("/", tags=["home"])  # La ruta del inicio (wrapper)
def message():
    return {"message": "Hello World"}


@app.get("/html", tags=["html"])  # La ruta del inicio (wrapper)
def htmlResponse():
    return HTMLResponse("<h1>Hello World</h1>")
