import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import (
    declarative_base,
)  # para manipular todas las tablas de la bd

sqlite_file_name = "../database.sqlite"
base_dir = os.path.dirname(os.path.realpath(__file__))

database_url = "sqlite:///" + os.path.join(base_dir, sqlite_file_name)

Engine = create_engine(database_url, echo=True)  # Hacemos el engine

Session = sessionmaker(bind=Engine)  # Creamos una sesion

Base = declarative_base()  # Creamos una base
