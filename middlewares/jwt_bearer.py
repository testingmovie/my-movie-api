from fastapi.security import HTTPBearer
from fastapi import Request, HTTPException, status
from utils.jwt_manager import validate_token


# Esto se aplica solamente a ciertas rutas por eso no se hace el app.add
class JWTBearer(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data["email"] != "admin@gmail.com":
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN, detail="Credenciales invalidas"
            )
