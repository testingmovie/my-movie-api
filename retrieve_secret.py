import os
from azure.keyvault.secrets import SecretClient
from azure.identity import DefaultAzureCredential


def set_api_key():
    keyVaultName = "denuncialo-key-vault"
    KVUri = f"https://{keyVaultName}.vault.azure.net"

    credential = DefaultAzureCredential()
    client = SecretClient(vault_url=KVUri, credential=credential)

    secretName = "denuncialo-api-key"
    retrieved_secret = client.get_secret(secretName)

    os.environ["API_KEY"] = retrieved_secret.value
